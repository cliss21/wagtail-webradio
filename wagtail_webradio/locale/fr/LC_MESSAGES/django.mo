��    X      �     �      �     �     �     �     �     �     �     �       -     0   A     r     �     �  
   �     �     �     �     �     �     �     �     �     
	     	  $   0	  [   U	     �	     �	  
   �	     �	     �	  D   �	     D
     G
     M
     R
     Z
     v
     �
     �
     �
     �
  
   �
  
   �
     �
            -     N     e  "   q     �     �  -   �  0   �          ,     I     O     c  	     R   �  h   �     E  	   b     l  D   x  )   �     �     �     �               $  
   A     L     Y     a     i     r  
        �     �  	   �  
   �     �     �     �  !  �     �     �     
  "         C     [     p     �  4   �  9   �               7     ?     N  	   S      ]     ~     �     �     �     �     �     �  '   �  ~         �     �     �     �     �  K   �     D     G     W     \     d     �  !   �     �     �     �     �  	          !   0  #   R     v  
   �     �     �     �  7   �  9   $     ^     s     �     �  #   �     �  h   �  �   J      �     �     �  C     -   O     }     �     �     �  !   �  "   �  
   �     �                      	   2  
   <     G  
   L     W     c     o     u     =           $   P       D            #   
      0   ,   K       A       9                T   *           8          G          >           /   B   5       :   H   W   N   E             C         (              4   ;   ?       3       M       F   .       7   U                O   	                   V   )         -   !               1                  R   Q   L                             &       S   "   %   J   I               <   6      X       +       '   2      @        Add Add a podcast Add a radio show Add a radio show permission Add podcast in Add podcasts Add radio show All radio shows Are you sure you want to delete this podcast? Are you sure you want to delete this radio show? Choose a podcast Choose another podcast Contact Currently: Date Delete Delete any podcast Description Download this song Duration Edit Edit any podcast Edit the radio show Edit this podcast Either a file or an URL is required. File type “%(mimetype)s” is not allowed. Allowed file types are: %(allowed_mimetypes)s. Go to the next song Go to the previous song Loading… Media No, don't delete One or more references to this object prevent it from being deleted: Or Pause Play Podcast Podcast '%(object)s' added. Podcast '%(object)s' deleted. Podcast '%(object)s' updated. Podcast tag Podcasts of Publish date Publishing Radio show Radio show '%(object)s' added. Radio show '%(object)s' deleted. Radio show '%(object)s' updated. Radio show permissions Radio shows Remove this song from the playlist Seek The format must be HH:MM:SS The podcast could not be saved due to errors. The radio show could not be saved due to errors. This field is required. This slug is already in use. Title Toggle the playback Toggle the playlist display Try again Unable to retrieve the duration of this file. Check that it is a valid audio file. Unable to validate the file at this URL. Check that it is a valid audio file by opening it in a new tab. View podcasts of '%(title)s' Web radio Yes, delete You cannot have multiple permission records for the same radio show. You must choose between a file or an URL. description duration email group group radio show permission group radio show permissions permission phone number picture podcast podcasts publish date radio show radio shows slug sound URL sound file tags title {model}: {object} Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-06-21 15:49+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.4.2
 Ajouter Ajouter un podcast Ajouter une émission Ajouter une permission d'émission Ajouter un podcast dans Ajouter des podcasts Ajouter une émission Toutes les émissions Êtes-vous sûr(e) de vouloir supprimer ce podcast ? Êtes-vous sûr(e) de vouloir supprimer cette émission ? Choisir un podcast Choisir un autre podcast Contact Actuellement : Date Supprimer Supprimer n'importe quel podcast Description Télécharger ce son Durée Modifier Modifier n'importe quel podcast Modifier l'émission Modifier ce podcast Soit un fichier ou une URL est requise. Le type de fichier « %(mimetype)s » n’est pas autorisé. Les types de fichier autorisés sont :  %(allowed_mimetypes)s. Aller au son suivant Aller au son précédent Chargement… Média Non, ne pas supprimer Une ou plusieurs références vers cet objet l'empêche d'être supprimé : Ou Mettre en pause Lire Podcast Podcast '%(object)s' ajouté. Podcast '%(object)s' supprimé. Podcast '%(object)s' mis à jour. Étiquette de podcast Podcasts de Date de publication Publication Émission Émission '%(object)s ajoutée. Émission '%(object)s supprimée. Émission '%(object)s mise à jour. Permissions d'émission Émissions Supprimer ce son de la playlist Naviguer dans le son Le format doit être HH:MM:SS Le podcast ne peut être enregistré du fait d'erreurs. L'émission ne peut être enregistrée du fait d'erreurs. Ce champ est requis. Ce slug est déjà utilisé. Titre Basculer la lecture Basculer l'affichage de la playlist Essayer à nouveau Impossible de récupérer la durée de ce fichier. Veuillez vérifier que c'est un fichier audio valide. Impossible de valider le fichier à cette URL. Veuillez vérifier que c'est un fichier audio valide en l'ouvrant dans un nouvel onglet. Voir les podcasts de '%(title)s' Webradio Oui, supprimer Vous ne pouvez avoir plusieurs permissions pour la même émission. Veuillez choisir entre un fichier et une URL. description durée courriel groupe permission d'émission par groupe permissions d'émission par groupe permission numéro de téléphone image podcast podcasts date de publication émission émissions slug URL du son fichier son étiquettes titre {model} : {object} 